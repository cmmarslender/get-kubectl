#!/usr/bin/env bash

# Copyright Chris Marslender
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# The install script is based off of the Apache-licensed script from helm,
# https://github.com/helm/helm/blob/master/scripts/get-helm-3

: ${BINARY_NAME:="kubectl"}
: ${USE_SUDO:="true"}
: ${KUBECTL_INSTALL_DIR:="/usr/local/bin"}

# initArch discovers the architecture for this system.
initArch() {
  ARCH=$(uname -m)
  case $ARCH in
    armv5*) ARCH="armv5";;
    armv6*) ARCH="armv6";;
    armv7*) ARCH="arm";;
    aarch64) ARCH="arm64";;
    x86) ARCH="386";;
    x86_64) ARCH="amd64";;
    i686) ARCH="386";;
    i386) ARCH="386";;
  esac
}

# initOS discovers the operating system for this system.
initOS() {
  OS=$(echo `uname`|tr '[:upper:]' '[:lower:]')

  case "$OS" in
    # Minimalist GNU for Windows
    mingw*) OS='windows';;
  esac
}

# runs the given command as root (detects if we are root already)
runAsRoot() {
  local CMD="$*"

  if [ $EUID -ne 0 -a $USE_SUDO = "true" ]; then
    CMD="sudo $CMD"
  fi

  $CMD
}

# verifySupported checks that the os/arch combination is supported for
# binary builds.
verifySupported() {
  local supported="darwin-386\ndarwin-amd64\nlinux-386\nlinux-amd64\nlinux-arm\nlinux-arm64\nlinux-ppc64le\nwindows-386\nwindows-amd64"
  if ! echo "${supported}" | grep -q "${OS}-${ARCH}"; then
    echo "No prebuilt binary for ${OS}-${ARCH}."
    exit 1
  fi

  if ! type "curl" > /dev/null && ! type "wget" > /dev/null; then
    echo "Either curl or wget is required"
    exit 1
  fi
}

# checkDesiredVersion checks if the desired version is available.
checkDesiredVersion() {
  if [ "x$DESIRED_VERSION" == "x" ]; then
    # Get tag from release URL
    local latest_release_url="https://storage.googleapis.com/kubernetes-release/release/stable.txt"
    if type "curl" > /dev/null; then
      TAG=$(curl -Ls $latest_release_url)
    elif type "wget" > /dev/null; then
      TAG=$(wget --quiet $latest_release_url -O - 2>&1)
    fi
  else
    TAG=$DESIRED_VERSION
  fi
}

# checkKubectlInstalledVersion checks which version of kubectl is installed and
# if it needs to be changed.
checkKubectlInstalledVersion() {
  if [[ -f "${KUBECTL_INSTALL_DIR}/${BINARY_NAME}" ]]; then
    local version=$("${KUBECTL_INSTALL_DIR}/${BINARY_NAME}" version --client --short | awk '{print $3}')
    if [[ "$version" == "$TAG" ]]; then
      echo "Kubectl ${version} is already ${DESIRED_VERSION:-latest}"
      return 0
    else
      echo "Kubectl ${TAG} is available. Changing from version ${version}."
      return 1
    fi
  else
    return 1
  fi
}

# downloadFile downloads the latest binary package and also the checksum
# for that binary.
downloadFile() {
  DOWNLOAD_URL="https://storage.googleapis.com/kubernetes-release/release/$TAG/bin/$OS/$ARCH/$BINARY_NAME"

  K8S_TMP_ROOT="$(mktemp -dt kubectl-installer-XXXXXX)"
  K8S_TMP_FILE="$K8S_TMP_ROOT/$BINARY_NAME"

  if type "curl" > /dev/null; then
    curl -SsL "$DOWNLOAD_URL" -o "$K8S_TMP_FILE"
  elif type "wget" > /dev/null; then
    wget -q -O "$K8S_TMP_FILE" "$DOWNLOAD_URL"
  fi
}

# installFile verifies the SHA256 for the file, then unpacks and
# installs it.
installFile() {
  K8S_TMP="$K8S_TMP_ROOT"

  mkdir -p "$K8S_TMP"
  K8S_TMP_BIN="$K8S_TMP/$BINARY_NAME"
  echo "Preparing to install $BINARY_NAME into ${KUBECTL_INSTALL_DIR}"
  runAsRoot cp "$K8S_TMP_BIN" "$KUBECTL_INSTALL_DIR/$BINARY_NAME"
  runAsRoot chmod +x "$KUBECTL_INSTALL_DIR/$BINARY_NAME"
  echo "$BINARY_NAME installed into $KUBECTL_INSTALL_DIR/$BINARY_NAME"
}

# fail_trap is executed if an error occurs.
fail_trap() {
  result=$?
  if [ "$result" != "0" ]; then
    if [[ -n "$INPUT_ARGUMENTS" ]]; then
      echo "Failed to install $BINARY_NAME with the arguments provided: $INPUT_ARGUMENTS"
      help
    else
      echo "Failed to install $BINARY_NAME"
    fi
  fi
  cleanup
  exit $result
}

# testVersion tests the installed client to make sure it is working.
testVersion() {
  set +e
  K8S="$(which $BINARY_NAME)"
  if [ "$?" = "1" ]; then
    echo "$BINARY_NAME not found. Is $KUBECTL_INSTALL_DIR on your "'$PATH?'
    exit 1
  fi
  set -e
}

# help provides possible cli installation arguments
help () {
  echo "Accepted cli arguments are:"
  echo -e "\t[--help|-h ] ->> prints this help"
  echo -e "\t[--version|-v <desired_version>] . When not defined it fetches the latest release from GitHub"
  echo -e "\te.g. --version v3.0.0 or -v canary"
  echo -e "\t[--no-sudo]  ->> install without sudo"
}

# cleanup temporary files to avoid https://github.com/helm/helm/issues/2977
cleanup() {
  if [[ -d "${K8S_TMP_ROOT:-}" ]]; then
    rm -rf "$K8S_TMP_ROOT"
  fi
}

# Execution

#Stop execution on any error
trap "fail_trap" EXIT
set -e

# Parsing input arguments (if any)
export INPUT_ARGUMENTS="${@}"
set -u
while [[ $# -gt 0 ]]; do
  case $1 in
    '--version'|-v)
       shift
       if [[ $# -ne 0 ]]; then
           export DESIRED_VERSION="${1}"
       else
           echo -e "Please provide the desired version. e.g. --version v3.0.0 or -v canary"
           exit 0
       fi
       ;;
    '--no-sudo')
       USE_SUDO="false"
       ;;
    '--help'|-h)
       help
       exit 0
       ;;
    *) exit 1
       ;;
  esac
  shift
done
set +u

initArch
initOS
verifySupported
checkDesiredVersion
if ! checkKubectlInstalledVersion; then
  downloadFile
  installFile
fi
testVersion
cleanup